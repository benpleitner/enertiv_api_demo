var margin = {top: 40, right: 20, bottom: 30, left: 40},
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

var x = d3.scale.ordinal()
    .rangeRoundBands([0, width], .1);

var y = d3.scale.linear()
    .range([height, 0]);

var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left")
    .outerTickSize(0)
    .ticks(10);

  //Hover feature
  var tip = d3.tip()
    .attr("class", "d3-tip")
    .offset([-10, 0])
    .html(function(d) {
      return "<strong>" + d.name + ":</strong> <span style='color:red'>" +
        d3.format(".4f")(d.value) + "</span>";
    });

  var svg = d3.select("body")
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  svg.call(tip);

  var header = svg.append("g")
    .append("text")
    .attr("x", 300)
    .attr("y", -5)
    .text("Average Energy Consumption May 22")
    .attr("font-size", "20px")
    .attr("font-weight", "bold");

d3.json("location_data.json", function (error, rawData) {
    if (error) {
    console.error(error);
    return;
  }

  var averages = {},
      maxAverage = 0,
      forGraph = [];

  rawData.forEach(function(d) {
    console.log(d);
    var name = d.name,
        data = d.data;

    var total = 0;
    var numEl = 0;
    for (var k = 0; k < data.length; k++) {
      total += data[k].y;
      numEl++;
    }

    var value = total / numEl;
    if (Object.keys(averages).indexOf(name) != -1) {
      averages[name] += value;
      if (averages[name] > maxAverage) {
        maxAverage = averages[name];
      }
    } else {
      averages[name] = value;
      if (averages[name] > maxAverage) {
        maxAverage = averages[name];
      }
    }
  });

  var arr = Object.keys(averages);
  for (var i = 0; i < arr.length; i++) {
    forGraph.push({name: arr[i],
                   value: averages[arr[i]]});    
  }

  x.domain(forGraph.map(function(d) { return d.name }));
  y.domain([0, maxAverage]);

  svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis);

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
      .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("kW");

  svg.selectAll(".bar")
    .data(forGraph)
    .enter().append("rect")
    .attr("class", "bar")
    .attr("x", function(d) { return x(d.name); })
    .attr("y", function(d) { return y(d.value); })
    .attr("width", x.rangeBand())
    .attr("height", function(d) { return height - y(d.value); })
    .on("mouseover", tip.show)
    .on("mouseout", tip.hide);
});
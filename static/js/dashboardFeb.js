//Angular
(function() {
  var app = angular.module('chart', []);

  app.controller('ChartController', function(){
    this.charts = data2;
  });

  app.controller('TabController', function(){
    this.tab = 1;

    this.setTab = function(newValue){
      this.tab = newValue;
    };

    this.isSet = function(tabName){
      return this.tab === tabName;
    };
  });

  app.controller('TabController1', function(){
    this.tab1 = 4;

    this.setTab = function(newValue){
      this.tab1 = newValue;
    };

    this.isSet = function(tabName){
      return this.tab1 === tabName;
    };
  });

  app.controller('MonthController', function(){
    this.month = 1;

    this.setMonth = function(newValue){
      this.month = newValue;
    };

    this.isSet = function(value){
      return this.month === value;
    };
  });

  app.controller('ButtonController', function(){
    this.button = 1;

    this.setButton = function(newValue){
      this.button = newValue;
    };

    this.isSet = function(tabName){
      return this.button === tabName;
    };
  });

  var data2 = [
    {
      totalCost: 100
    }
  ];
})();

d3.json("/location/data?location_uuid=8639f0e6-1623-4e60-8164-a853fb807917&q_string=fromTime%3D2015-02-01%2005:00:00Z%26toTime%3D2015-03-01%2004:00:00Z%26interval%3Dhour%26aggregate%3dfalse%26data_format%3Drickshaw", function(error, rawData) {
  //Totals
  var totals = {},
      costs = {},
      maxTotal = 0,
      forGraph = [];

  rawData.forEach(function(d) {
    var name = d.sublocation_name,
        data = d.data;

    var total = 0;
    var totalC = 0;
    for (var k = 0; k < data.length; k++) {
      total += data[k].y;
      totalC += data[k].total_cost;
    }

    if (Object.keys(totals).indexOf(name) != -1) {
      totals[name] += total;
      if (totals[name] > maxTotal) {
        maxTotal = totals[name];
      }
    } else {
      totals[name] = total;
      if (totals[name] > maxTotal) {
        maxTotal = totals[name];
      }
    }

    if (Object.keys(costs).indexOf("" + name) != -1) {
      costs[name] += totalC;
    } else {
      costs[name] = totalC;
    }
  });

  var maxIndex = 0;

  var arr = Object.keys(totals);
  for (var i = 0; i < arr.length; i++) {
    maxIndex = arr.length - 1;

    forGraph.push({ name: arr[i],
                    index: i,
                    value: totals[arr[i]],
                    cost: costs[arr[i]]});   
  }

  //HTML Output
  var totalCostA = 0;
  var totalKwA = 0;
  for (var z = 0; z < forGraph.length; z++) {
    totalCostA += forGraph[z].cost;
    totalKwA += forGraph[z].value;
  }

  document.getElementById("totalCostFeb").innerHTML = totalCostA.toFixed(2);

  var monthlyCost = "";
  for (var z = 0; z < forGraph.length; z++) {
    monthlyCost = forGraph[z].name + ": $" + forGraph[z].cost.toFixed(2) +
      "<span> (" + (forGraph[z].cost/totalCostA * 100).toFixed(2) + "% of total cost)</span>";
    d3.select("#monthlyCostFeb")
      .append("div")
      .attr("id", "h4div")
      .html(monthlyCost);
  }

  document.getElementById("totalKwFeb").innerHTML = totalKwA.toFixed(2);

  var monthlyKw = "";
  for (var z = 0; z < forGraph.length; z++) {
    monthlyKw = forGraph[z].name + ": " + forGraph[z].value.toFixed(2) + " kW" +
    "<span> (" + (forGraph[z].value/totalKwA * 100).toFixed(2) + "% of total usage)</span>";
    d3.select("#monthlyKwFeb")
      .append("div")
      .attr("id", "h4div")
      .html(monthlyKw);
  }










  var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";

  var date = [];

  //Dates
  rawData.forEach(function(d) {
    d.data.forEach(function (d, i) {
      var time = d.x;
      var d_date = new Date(0);
      d_date.setUTCSeconds(time);
      d.x = d_date.getDate();
      d.month = month[d_date.getMonth()];
    });
  });

  var dateTotal = {},
      dateCost = {},
      months = {},
      max = 0,
      minDate = rawData[0].data[0].x;
      maxDate = rawData[rawData.length - 1].data[rawData[rawData.length - 1].data.length - 1].x;
      forChart = [];

  rawData.forEach(function(d) {
    var name1 = d.sublocation_name

    d.data.forEach(function (d, i) {
      var name = d.x, //Change to date
          output = d.y;
          month = d.month;
          cost = d.total_cost;

      if (name < minDate) {
        minDate = name;
      }
      if (name > maxDate) {
        maxDate = name;
      }

      if (Object.keys(dateTotal).indexOf("" + name) != -1) {
        dateTotal[name] += output;
        if (dateTotal[name] > max) {
          max = dateTotal[name];
        }
      } else {
        dateTotal[name] = output;
        months[name] = month;
        if (dateTotal[name] > max) {
          max = dateTotal[name];
        }
      }
      if (Object.keys(dateCost).indexOf("" + name) != -1) {
        dateCost[name] += cost;
      } else {
        dateCost[name] = cost;
      }
    });
  });

  var arr = Object.keys(dateTotal);
  for (var i = 0; i < arr.length; i++) {
    forChart.push({ date: arr[i],
                    month: months[arr[i]],
                    value: dateTotal[arr[i]],
                    cost: dateCost[arr[i]]
                  });    
  }

  //Get weekly data
  var weekly = [];
  var weekRanges = [];

  for (var a = 0; a < forChart.length / 7; a++) {
    var name = "week " + a;
    weekly[a] = [];
    var minR = forChart[a * 7].month + " " + forChart[a * 7].date;
    var maxR;

    for (var b = 0; b < forChart.length; b++) {
      if (b == (a + 1) * 7 - 1 || a > 3 && b == forChart.length - 1) {
        weekRanges.push({ range: minR + " - " + forChart[b].date });
      }
      if (b >= (a * 7) && b < (a + 1) * 7) {
        weekly[a].push({ date: forChart[b].date,
                         value: forChart[b].value
                       });
      }
    }
  }








  //Crossfilters
  var data = forChart;
  var data1 = forGraph;

  var ndx = crossfilter(data);
  var ndx1 = crossfilter(data1);

  var dates = ndx.dimension(function(d) {
    return parseInt(d.date);
  });

  var kW = dates.group().reduceSum(function(d) {
    return d.value;
  });

  var cost = dates.group().reduceSum(function(d) {
    return d.cost;
  });

  var dates1  = ndx.dimension(function(d, i) {
    return parseInt(d.date);
  });

  var kW1 = dates1.group().reduceSum(function(d) {
    return d.value;
  });

  //Bar Graph
  barChartF = dc.barChart("#barChartF");
  
  barChartF.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(dates)
        .group(kW)
        .elasticY(true)
        .centerBar(true)
        .gap(1)
        .x(d3.scale.linear().domain([minDate - 1, maxDate + 1]))
        .yAxisLabel("kW")
        .xAxisLabel("Date")
        .renderHorizontalGridLines(true)
        .transitionDuration(700);

  barChartF.xAxis().tickFormat(function (v) {
    if (v == minDate - 1 || v == maxDate + 1) {
      return "";
    }
    return data[v].month + " " + v;
  });

  //Line Graph
  lineChartF = dc.lineChart("#lineChartF");

  lineChartF.width(800 * 0.8)
    .height(350 * 0.8)
    .margins({top: 10, right: 50, bottom: 30, left: 40})
    .dimension(dates)
    .group(cost, "cost")
    .x(d3.scale.linear().domain([minDate, maxDate]))
    .legend(dc.legend().x(60).y(10).itemHeight(12).gap(5))
    .transitionDuration(700)
    .yAxisLabel("US Dollars")
    .xAxisLabel("Date");

  lineChartF.xAxis().ticks(7);

  lineChartF.xAxis().tickFormat(function (v) {
    if (v == minDate - 1 || v == maxDate + 1) {
      return "";
    }
    return data[v].month + " " + v;
  });

  // //Pie Chart
  // pieChart = dc.pieChart("#pieChart"); //CHANGE TO A BUBBLE CHART
  
  // pieChart.width(800 * 0.8)
  //     .height(400 * 0.8)
  //     .dimension(datesWeek1)
  //     .group(kwWeek1)
  //     .label(function (d) {
  //       return "May " + d.key;
  //     })
  //     .transitionDuration(700);

  bubbleChart1F = dc.bubbleChart("#pieChartF")

  bubbleChart1F.width(800 * 1.6)
          .height(350 * 1)
          .margins({top: 10, right: 50, bottom: 30, left: 40})
          .transitionDuration(700)
          .dimension(dates1)
          .group(kW1)
          .x(d3.scale.linear().domain([minDate - 1, maxDate + 1]))
          .y(d3.scale.linear().domain([0, 450]))
          .radiusValueAccessor(function(p) { return p.value; })
          .r(d3.scale.linear().domain([0, 10000]))
          .label(function(d) { return "" })
          .yAxisLabel("kW")
          .xAxisLabel("Date")

  bubbleChart1F.xAxis().tickFormat(function (v) {
    if (v == minDate - 1 || v == maxDate + 1) {
      return "";
    }
    return data[v].month + " " + v;
  });



  var rooms = ndx1.dimension(function(d) {
    return d.index;
  });

  var kW = rooms.group().reduceSum(function(d) {
    return d.value
  });

  var rooms2 = ndx1.dimension(function(d) {
    return d.index;
  });

  var cost = rooms2.group().reduceSum(function(d) {
    return d.cost
  });

  var rooms1 = {}
  for (var i = 0; i < data1.length; i++) {
    rooms1[i] = data1[i].name;
  }

/*
  THIS IS FOR KW
*/
  //Second Bar Chart
  barChart1 = dc.barChart("#barChart1F");
  
  barChart1.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(rooms)
        .group(kW)
        .elasticY(true)
        .centerBar(true)
        .gap(1)
        .x(d3.scale.linear().domain([-1, maxIndex + 1]))
        .yAxisLabel("kW")
        .xAxisLabel("Room")
        .renderHorizontalGridLines(true)
        .transitionDuration(700);

  barChart1.xAxis().tickFormat(function (v) {
    if (v == -1 || v == maxIndex + 1) {
      return "";
    }
    return rooms1[v];
  });

  //Bubble Chart
  bubbleChart = dc.bubbleChart("#bubbleChartF")

  bubbleChart.width(800 * 0.8)
          .height(350 * 0.8)
          .margins({top: 10, right: 50, bottom: 30, left: 40})
          .transitionDuration(700)
          .dimension(rooms)
          .group(kW)
          .x(d3.scale.linear().domain([-1, maxIndex + 1]))
          .y(d3.scale.linear().domain([-100, 3000]))
          .radiusValueAccessor(function(p) { return p.value; })
          .r(d3.scale.linear().domain([0, 10000]))
          .label(function(d) { return "" })
          .yAxisLabel("kW")
          .xAxisLabel("Room");

  bubbleChart.xAxis().tickFormat(function (v) {
    if (v == -1 || v == maxIndex + 1) {
      return "";
    }
    return rooms1[v];
  });

  //RowChart
  rowChart1 = dc.rowChart("#rowChartF"); //ADD AN AXIS LABEL

  rowChart1.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(rooms)
        .group(kW)
        .elasticX(true)
        .gap(1)
        .x(d3.scale.linear().domain([-1, maxIndex + 1]))
        .label(function(d) { return rooms1[d.key] })
        .transitionDuration(700);

/*
  THIS IS FOR COST
*/
  //Second Bar Chart
  barChart12 = dc.barChart("#barChart12F");
  
  barChart12.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(rooms2)
        .group(cost)
        .elasticY(true)
        .centerBar(true)
        .gap(1)
        .x(d3.scale.linear().domain([-1, maxIndex + 1]))
        .yAxisLabel("US Dollars")
        .xAxisLabel("Room")
        .renderHorizontalGridLines(true)
        .transitionDuration(700);

  barChart12.xAxis().tickFormat(function (v) {
    if (v == -1 || v == maxIndex + 1) {
      return "";
    }
    return rooms1[v];
  });

  //Bubble Chart
  bubbleChart2 = dc.bubbleChart("#bubbleChart2F")

  bubbleChart2.width(800 * 0.8)
          .height(350 * 0.8)
          .margins({top: 10, right: 50, bottom: 30, left: 40})
          .transitionDuration(700)
          .dimension(rooms2)
          .group(cost)
          .x(d3.scale.linear().domain([-1, maxIndex + 1]))
          .y(d3.scale.linear().domain([-50, 600]))
          .radiusValueAccessor(function(p) { return p.value; })
          .r(d3.scale.linear().domain([0, 10000]))
          .label(function(d) { return "" })
          .yAxisLabel("US Dollars")
          .xAxisLabel("Room");

  bubbleChart2.xAxis().tickFormat(function (v) {
    if (v == -1 || v == maxIndex + 1) {
      return "";
    }
    return rooms1[v];
  });

  //RowChart
  rowChart12 = dc.rowChart("#rowChart2F"); //ADD AN AXIS LABEL

  rowChart12.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(rooms2)
        .group(cost)
        .elasticX(true)
        .gap(1)
        .x(d3.scale.linear().domain([-1, maxIndex + 1]))
        .label(function(d) { return rooms1[d.key] })
        .transitionDuration(700);

  dc.renderAll();
});
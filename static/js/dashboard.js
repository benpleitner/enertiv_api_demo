//Angular
(function() {
  var app = angular.module('chart', []);

  app.controller('ChartController', function(){
    this.charts = data2;
  });

  app.controller('TabController', function(){
    this.tab = 1;

    this.setTab = function(newValue){
      this.tab = newValue;
    };

    this.isSet = function(tabName){
      return this.tab === tabName;
    };
  });

  app.controller('TabController1', function(){
    this.tab1 = 4;

    this.setTab = function(newValue){
      this.tab1 = newValue;
    };

    this.isSet = function(tabName){
      return this.tab1 === tabName;
    };
  });

  app.controller('MonthController', function(){
    this.month = 1;

    this.setMonth = function(newValue){
      this.month = newValue;
    };

    this.isSet = function(value){
      return this.month === value;
    };
  });

  app.controller('ButtonController', function(){
    this.button = 1;

    this.setButton = function(newValue){
      this.button = newValue;
    };

    this.isSet = function(tabName){
      return this.button === tabName;
    };
  });

  var data2 = [
    {
      totalCost: 100
    }
  ];
})();

//Comparison calculations
var mayTotalCost = 0,
    mayTotalKw = 0,
    toMayTotalCost = 0,
    toMayTotalKw = 0,
    janFebTotalCost = 0,
    janFebTotalKw = 0,
    febMarTotalCost = 0,
    febMarTotalKw = 0,
    marAprTotalCost = 0,
    marAprTotalKw = 0,
    janTotalCost = 0,
    janTotalKw = 0,
    febTotalCost = 0,
    febTotalKw = 0,
    marTotalCost = 0,
    marTotalKw = 0,
    aprTotalCost = 0,
    aprTotalKw = 0;

var monthData = [];

/* 


MAY


*/
d3.json("/location/data?location_uuid=8639f0e6-1623-4e60-8164-a853fb807917&q_string=fromTime%3D2015-05-01%2005:00:00Z%26toTime%3D2015-06-01%2004:00:00Z%26interval%3Dhour%26aggregate%3dfalse%26data_format%3Drickshaw", function(error, rawData) {
  //Totals
  var totals = {},
      costs = {},
      maxTotal = 0,
      forGraph = [];

  rawData.forEach(function(d) {
    var name = d.sublocation_name,
        data = d.data;

    var total = 0;
    var totalC = 0;
    for (var k = 0; k < data.length; k++) {
      total += data[k].y;
      totalC += data[k].total_cost;
    }

    if (Object.keys(totals).indexOf(name) != -1) {
      totals[name] += total;
      if (totals[name] > maxTotal) {
        maxTotal = totals[name];
      }
    } else {
      totals[name] = total;
      if (totals[name] > maxTotal) {
        maxTotal = totals[name];
      }
    }

    if (Object.keys(costs).indexOf("" + name) != -1) {
      costs[name] += totalC;
    } else {
      costs[name] = totalC;
    }
  });

  var maxIndex = 0;

  var arr = Object.keys(totals);
  for (var i = 0; i < arr.length; i++) {
    maxIndex = arr.length - 1;

    forGraph.push({ name: arr[i],
                    index: i,
                    value: totals[arr[i]],
                    cost: costs[arr[i]]});   
  }

  //HTML Output
  var totalCostA = 0;
  var totalKwA = 0;
  for (var z = 0; z < forGraph.length; z++) {
    totalCostA += forGraph[z].cost;
    totalKwA += forGraph[z].value;
  }
  mayTotalCost = totalCostA;
  mayTotalKw = totalKwA;

  document.getElementById("totalCost").innerHTML = totalCostA.toFixed(2);

  var monthlyCost = "";
  for (var z = 0; z < forGraph.length; z++) {
    monthlyCost = forGraph[z].name + ": $" + forGraph[z].cost.toFixed(2) +
      "<span> (" + (forGraph[z].cost/totalCostA * 100).toFixed(2) + "% of total cost)</span>";
    d3.select("#monthlyCost")
      .append("div")
      .attr("id", "h4div")
      .html(monthlyCost);
  }

  document.getElementById("totalKw").innerHTML = totalKwA.toFixed(2);

  var monthlyKw = "";
  for (var z = 0; z < forGraph.length; z++) {
    monthlyKw = forGraph[z].name + ": " + forGraph[z].value.toFixed(2) + " kW" +
    "<span> (" + (forGraph[z].value/totalKwA * 100).toFixed(2) + "% of total usage)</span>";
    d3.select("#monthlyKw")
      .append("div")
      .attr("id", "h4div")
      .html(monthlyKw);
  }










  var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";

  var date = [];

  //Dates
  rawData.forEach(function(d) {
    d.data.forEach(function (d, i) {
      var time = d.x;
      var d_date = new Date(0);
      d_date.setUTCSeconds(time);
      d.x = d_date.getDate();
      d.month = month[d_date.getMonth()];
    });
  });

  var dateTotal = {},
      dateCost = {},
      months = {},
      max = 0,
      minDate = rawData[0].data[0].x;
      maxDate = rawData[rawData.length - 1].data[rawData[rawData.length - 1].data.length - 1].x;
      forChart = [];

  rawData.forEach(function(d) {
    var name1 = d.sublocation_name

    d.data.forEach(function (d, i) {
      var name = d.x, //Change to date
          output = d.y;
          month = d.month;
          cost = d.total_cost;

      if (name < minDate) {
        minDate = name;
      }
      if (name > maxDate) {
        maxDate = name;
      }

      if (Object.keys(dateTotal).indexOf("" + name) != -1) {
        dateTotal[name] += output;
        if (dateTotal[name] > max) {
          max = dateTotal[name];
        }
      } else {
        dateTotal[name] = output;
        months[name] = month;
        if (dateTotal[name] > max) {
          max = dateTotal[name];
        }
      }
      if (Object.keys(dateCost).indexOf("" + name) != -1) {
        dateCost[name] += cost;
      } else {
        dateCost[name] = cost;
      }
    });
  });

  var arr = Object.keys(dateTotal);
  for (var i = 0; i < arr.length; i++) {
    forChart.push({ date: arr[i],
                    month: months[arr[i]],
                    value: dateTotal[arr[i]],
                    cost: dateCost[arr[i]]
                  });    
  }

  //Get weekly data
  var weekly = [];
  var weekRanges = [];

  for (var a = 0; a < forChart.length / 7; a++) {
    var name = "week " + a;
    weekly[a] = [];
    var minR = forChart[a * 7].month + " " + forChart[a * 7].date;
    var maxR;

    for (var b = 0; b < forChart.length; b++) {
      if (b == (a + 1) * 7 - 1 || a > 3 && b == forChart.length - 1) {
        weekRanges.push({ range: minR + " - " + forChart[b].date });
      }
      if (b >= (a * 7) && b < (a + 1) * 7) {
        weekly[a].push({ date: forChart[b].date,
                         value: forChart[b].value
                       });
      }
    }
  }


  //Add the monthly data
  monthData.push({month: forChart[0].month,
                  data: forChart});

  //Crossfilters
  var data = forChart;
  var data1 = forGraph;

  var ndx = crossfilter(data);
  var ndx1 = crossfilter(data1);

  var dates = ndx.dimension(function(d) {
    return parseInt(d.date);
  });

  var kW = dates.group().reduceSum(function(d) {
    return d.value;
  });

  var cost = dates.group().reduceSum(function(d) {
    return d.cost;
  });

  var dates1  = ndx.dimension(function(d, i) {
    return parseInt(d.date);
  });

  var kW1 = dates1.group().reduceSum(function(d) {
    return d.value;
  });

  //Bar Graph
  barChart = dc.barChart("#barChart");
  
  barChart.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(dates)
        .group(kW)
        .elasticY(true)
        .centerBar(true)
        .gap(1)
        .x(d3.scale.linear().domain([minDate - 1, maxDate + 1]))
        .yAxisLabel("kW")
        .xAxisLabel("Date")
        .renderHorizontalGridLines(true)
        .transitionDuration(700);

  barChart.xAxis().tickFormat(function (v) {
    if (v == minDate - 1 || v == maxDate + 1) {
      return "";
    }
    return data[v].month + " " + v;
  });

  //Line Graph
  lineChart = dc.lineChart("#lineChart");

  lineChart.width(800 * 0.8)
    .height(350 * 0.8)
    .margins({top: 10, right: 50, bottom: 30, left: 40})
    .dimension(dates)
    .group(cost, "cost")
    .x(d3.scale.linear().domain([minDate, maxDate]))
    .legend(dc.legend().x(60).y(10).itemHeight(12).gap(5))
    .transitionDuration(700)
    .yAxisLabel("US Dollars")
    .xAxisLabel("Date");

  lineChart.xAxis().ticks(7);

  lineChart.xAxis().tickFormat(function (v) {
    if (v == minDate - 1 || v == maxDate + 1) {
      return "";
    }
    return data[v].month + " " + v;
  });

  // //Pie Chart
  // pieChart = dc.pieChart("#pieChart"); //CHANGE TO A BUBBLE CHART
  
  // pieChart.width(800 * 0.8)
  //     .height(400 * 0.8)
  //     .dimension(datesWeek1)
  //     .group(kwWeek1)
  //     .label(function (d) {
  //       return "May " + d.key;
  //     })
  //     .transitionDuration(700);

  bubbleChart1 = dc.bubbleChart("#pieChart")

  bubbleChart1.width(800 * 1.6)
          .height(350 * 1)
          .margins({top: 10, right: 50, bottom: 30, left: 40})
          .transitionDuration(700)
          .dimension(dates1)
          .group(kW1)
          .x(d3.scale.linear().domain([minDate - 1, maxDate + 1]))
          .y(d3.scale.linear().domain([0, 450]))
          .radiusValueAccessor(function(p) { return p.value; })
          .r(d3.scale.linear().domain([0, 10000]))
          .label(function(d) { return "" })
          .yAxisLabel("kW")
          .xAxisLabel("Date")

  bubbleChart1.xAxis().tickFormat(function (v) {
    if (v == minDate - 1 || v == maxDate + 1) {
      return "";
    }
    return data[v].month + " " + v;
  });



  var rooms = ndx1.dimension(function(d) {
    return d.index;
  });

  var kW = rooms.group().reduceSum(function(d) {
    return d.value
  });

  var rooms2 = ndx1.dimension(function(d) {
    return d.index;
  });

  var cost = rooms2.group().reduceSum(function(d) {
    return d.cost
  });

  var rooms1 = {}
  for (var i = 0; i < data1.length; i++) {
    rooms1[i] = data1[i].name;
  }

/*
  THIS IS FOR KW
*/
  //Second Bar Chart
  barChart1 = dc.barChart("#barChart1");
  
  barChart1.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(rooms)
        .group(kW)
        .elasticY(true)
        .centerBar(true)
        .gap(1)
        .x(d3.scale.linear().domain([-1, maxIndex + 1]))
        .yAxisLabel("kW")
        .xAxisLabel("Room")
        .renderHorizontalGridLines(true)
        .transitionDuration(700);

  barChart1.xAxis().tickFormat(function (v) {
    if (v == -1 || v == maxIndex + 1) {
      return "";
    }
    return rooms1[v];
  });

  //Bubble Chart
  bubbleChart = dc.bubbleChart("#bubbleChart")

  bubbleChart.width(800 * 0.8)
          .height(350 * 0.8)
          .margins({top: 10, right: 50, bottom: 30, left: 40})
          .transitionDuration(700)
          .dimension(rooms)
          .group(kW)
          .x(d3.scale.linear().domain([-1, maxIndex + 1]))
          .y(d3.scale.linear().domain([-100, 3000]))
          .radiusValueAccessor(function(p) { return p.value; })
          .r(d3.scale.linear().domain([0, 10000]))
          .label(function(d) { return "" })
          .yAxisLabel("kW")
          .xAxisLabel("Room");

  bubbleChart.xAxis().tickFormat(function (v) {
    if (v == -1 || v == maxIndex + 1) {
      return "";
    }
    return rooms1[v];
  });

  //RowChart
  rowChart1 = dc.rowChart("#rowChart"); //ADD AN AXIS LABEL

  rowChart1.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(rooms)
        .group(kW)
        .elasticX(true)
        .gap(1)
        .x(d3.scale.linear().domain([-1, maxIndex + 1]))
        .label(function(d) { return rooms1[d.key] })
        .transitionDuration(700);

/*
  THIS IS FOR COST
*/
  //Second Bar Chart
  barChart12 = dc.barChart("#barChart12");
  
  barChart12.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(rooms2)
        .group(cost)
        .elasticY(true)
        .centerBar(true)
        .gap(1)
        .x(d3.scale.linear().domain([-1, maxIndex + 1]))
        .yAxisLabel("US Dollars")
        .xAxisLabel("Room")
        .renderHorizontalGridLines(true)
        .transitionDuration(700);

  barChart12.xAxis().tickFormat(function (v) {
    if (v == -1 || v == maxIndex + 1) {
      return "";
    }
    return rooms1[v];
  });

  //Bubble Chart
  bubbleChart2 = dc.bubbleChart("#bubbleChart2")

  bubbleChart2.width(800 * 0.8)
          .height(350 * 0.8)
          .margins({top: 10, right: 50, bottom: 30, left: 40})
          .transitionDuration(700)
          .dimension(rooms2)
          .group(cost)
          .x(d3.scale.linear().domain([-1, maxIndex + 1]))
          .y(d3.scale.linear().domain([-50, 600]))
          .radiusValueAccessor(function(p) { return p.value; })
          .r(d3.scale.linear().domain([0, 10000]))
          .label(function(d) { return "" })
          .yAxisLabel("US Dollars")
          .xAxisLabel("Room");

  bubbleChart2.xAxis().tickFormat(function (v) {
    if (v == -1 || v == maxIndex + 1) {
      return "";
    }
    return rooms1[v];
  });

  //RowChart
  rowChart12 = dc.rowChart("#rowChart2"); //ADD AN AXIS LABEL

  rowChart12.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(rooms2)
        .group(cost)
        .elasticX(true)
        .gap(1)
        .x(d3.scale.linear().domain([-1, maxIndex + 1]))
        .label(function(d) { return rooms1[d.key] })
        .transitionDuration(700);

  dc.renderAll();
});















/* 


JANUARY


*/
d3.json("/location/data?location_uuid=8639f0e6-1623-4e60-8164-a853fb807917&q_string=fromTime%3D2015-01-01%2005:00:00Z%26toTime%3D2015-02-01%2004:00:00Z%26interval%3Dhour%26aggregate%3dfalse%26data_format%3Drickshaw", function(error, rawData) {
  //Totals
  var totals = {},
      costs = {},
      maxTotal = 0,
      forGraph = [];

  rawData.forEach(function(d) {
    var name = d.sublocation_name,
        data = d.data;

    var total = 0;
    var totalC = 0;
    for (var k = 0; k < data.length; k++) {
      total += data[k].y;
      totalC += data[k].total_cost;
    }

    if (Object.keys(totals).indexOf(name) != -1) {
      totals[name] += total;
      if (totals[name] > maxTotal) {
        maxTotal = totals[name];
      }
    } else {
      totals[name] = total;
      if (totals[name] > maxTotal) {
        maxTotal = totals[name];
      }
    }

    if (Object.keys(costs).indexOf("" + name) != -1) {
      costs[name] += totalC;
    } else {
      costs[name] = totalC;
    }
  });

  var maxIndex = 0;

  var arr = Object.keys(totals);
  for (var i = 0; i < arr.length; i++) {
    maxIndex = arr.length - 1;

    forGraph.push({ name: arr[i],
                    index: i,
                    value: totals[arr[i]],
                    cost: costs[arr[i]]});   
  }

  //HTML Output
  var totalCostA = 0;
  var totalKwA = 0;
  for (var z = 0; z < forGraph.length; z++) {
    totalCostA += forGraph[z].cost;
    totalKwA += forGraph[z].value;
  }
  janTotalCost = totalCostA;
  janTotalKw = totalKwA;

  document.getElementById("totalCostJan").innerHTML = totalCostA.toFixed(2);

  var monthlyCost = "";
  for (var z = 0; z < forGraph.length; z++) {
    monthlyCost = forGraph[z].name + ": $" + forGraph[z].cost.toFixed(2) +
      "<span> (" + (forGraph[z].cost/totalCostA * 100).toFixed(2) + "% of total cost)</span>";
    d3.select("#monthlyCostJan")
      .append("div")
      .attr("id", "h4div")
      .html(monthlyCost);
  }

  document.getElementById("totalKwJan").innerHTML = totalKwA.toFixed(2);

  var monthlyKw = "";
  for (var z = 0; z < forGraph.length; z++) {
    monthlyKw = forGraph[z].name + ": " + forGraph[z].value.toFixed(2) + " kW" +
    "<span> (" + (forGraph[z].value/totalKwA * 100).toFixed(2) + "% of total usage)</span>";
    d3.select("#monthlyKwJan")
      .append("div")
      .attr("id", "h4div")
      .html(monthlyKw);
  }










  var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";

  var date = [];

  //Dates
  rawData.forEach(function(d) {
    d.data.forEach(function (d, i) {
      var time = d.x;
      var d_date = new Date(0);
      d_date.setUTCSeconds(time);
      d.x = d_date.getDate();
      d.month = month[d_date.getMonth()];
    });
  });

  var dateTotal = {},
      dateCost = {},
      months = {},
      max = 0,
      minDate = rawData[0].data[0].x;
      maxDate = rawData[rawData.length - 1].data[rawData[rawData.length - 1].data.length - 1].x;
      forChart = [];

  rawData.forEach(function(d) {
    var name1 = d.sublocation_name

    d.data.forEach(function (d, i) {
      var name = d.x, //Change to date
          output = d.y;
          month = d.month;
          cost = d.total_cost;

      if (name < minDate) {
        minDate = name;
      }
      if (name > maxDate) {
        maxDate = name;
      }

      if (Object.keys(dateTotal).indexOf("" + name) != -1) {
        dateTotal[name] += output;
        if (dateTotal[name] > max) {
          max = dateTotal[name];
        }
      } else {
        dateTotal[name] = output;
        months[name] = month;
        if (dateTotal[name] > max) {
          max = dateTotal[name];
        }
      }
      if (Object.keys(dateCost).indexOf("" + name) != -1) {
        dateCost[name] += cost;
      } else {
        dateCost[name] = cost;
      }
    });
  });

  var arr = Object.keys(dateTotal);
  for (var i = 0; i < arr.length; i++) {
    forChart.push({ date: arr[i],
                    month: months[arr[i]],
                    value: dateTotal[arr[i]],
                    cost: dateCost[arr[i]]
                  });    
  }

  //Get weekly data
  var weekly = [];
  var weekRanges = [];

  for (var a = 0; a < forChart.length / 7; a++) {
    var name = "week " + a;
    weekly[a] = [];
    var minR = forChart[a * 7].month + " " + forChart[a * 7].date;
    var maxR;

    for (var b = 0; b < forChart.length; b++) {
      if (b == (a + 1) * 7 - 1 || a > 3 && b == forChart.length - 1) {
        weekRanges.push({ range: minR + " - " + forChart[b].date });
      }
      if (b >= (a * 7) && b < (a + 1) * 7) {
        weekly[a].push({ date: forChart[b].date,
                         value: forChart[b].value
                       });
      }
    }
  }




  //Add the monthly data
  monthData.push({month: forChart[0].month,
                  data: forChart});

  //Crossfilters
  var data = forChart;
  var data1 = forGraph;

  var ndx = crossfilter(data);
  var ndx1 = crossfilter(data1);

  var dates = ndx.dimension(function(d) {
    return parseInt(d.date);
  });

  var kW = dates.group().reduceSum(function(d) {
    return d.value;
  });

  var cost = dates.group().reduceSum(function(d) {
    return d.cost;
  });

  var dates1  = ndx.dimension(function(d, i) {
    return parseInt(d.date);
  });

  var kW1 = dates1.group().reduceSum(function(d) {
    return d.value;
  });

  //Bar Graph
  barChartJ = dc.barChart("#barChartJ");
  
  barChartJ.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(dates)
        .group(kW)
        .elasticY(true)
        .centerBar(true)
        .gap(1)
        .x(d3.scale.linear().domain([minDate - 1, maxDate + 1]))
        .yAxisLabel("kW")
        .xAxisLabel("Date")
        .renderHorizontalGridLines(true)
        .transitionDuration(700);

  barChartJ.xAxis().tickFormat(function (v) {
    if (v == minDate - 1 || v == maxDate + 1) {
      return "";
    }
    return data[v].month + " " + v;
  });

  //Line Graph
  lineChartJ = dc.lineChart("#lineChartJ");

  lineChartJ.width(800 * 0.8)
    .height(350 * 0.8)
    .margins({top: 10, right: 50, bottom: 30, left: 40})
    .dimension(dates)
    .group(cost, "cost")
    .x(d3.scale.linear().domain([minDate, maxDate]))
    .legend(dc.legend().x(60).y(10).itemHeight(12).gap(5))
    .transitionDuration(700)
    .yAxisLabel("US Dollars")
    .xAxisLabel("Date");

  lineChartJ.xAxis().ticks(7);

  lineChartJ.xAxis().tickFormat(function (v) {
    if (v == minDate - 1 || v == maxDate + 1) {
      return "";
    }
    return data[v].month + " " + v;
  });

  // //Pie Chart
  // pieChart = dc.pieChart("#pieChart"); //CHANGE TO A BUBBLE CHART
  
  // pieChart.width(800 * 0.8)
  //     .height(400 * 0.8)
  //     .dimension(datesWeek1)
  //     .group(kwWeek1)
  //     .label(function (d) {
  //       return "May " + d.key;
  //     })
  //     .transitionDuration(700);

  bubbleChart1J = dc.bubbleChart("#pieChartJ")

  bubbleChart1J.width(800 * 1.6)
          .height(350 * 1)
          .margins({top: 10, right: 50, bottom: 30, left: 40})
          .transitionDuration(700)
          .dimension(dates1)
          .group(kW1)
          .x(d3.scale.linear().domain([minDate - 1, maxDate + 1]))
          .y(d3.scale.linear().domain([0, 450]))
          .radiusValueAccessor(function(p) { return p.value; })
          .r(d3.scale.linear().domain([0, 10000]))
          .label(function(d) { return "" })
          .yAxisLabel("kW")
          .xAxisLabel("Date")

  bubbleChart1J.xAxis().tickFormat(function (v) {
    if (v == minDate - 1 || v == maxDate + 1) {
      return "";
    }
    return data[v].month + " " + v;
  });



  var rooms = ndx1.dimension(function(d) {
    return d.index;
  });

  var kW = rooms.group().reduceSum(function(d) {
    return d.value
  });

  var rooms2 = ndx1.dimension(function(d) {
    return d.index;
  });

  var cost = rooms2.group().reduceSum(function(d) {
    return d.cost
  });

  var rooms1 = {}
  for (var i = 0; i < data1.length; i++) {
    rooms1[i] = data1[i].name;
  }

/*
  THIS IS FOR KW
*/
  //Second Bar Chart
  barChart1 = dc.barChart("#barChart1J");
  
  barChart1.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(rooms)
        .group(kW)
        .elasticY(true)
        .centerBar(true)
        .gap(1)
        .x(d3.scale.linear().domain([-1, maxIndex + 1]))
        .yAxisLabel("kW")
        .xAxisLabel("Room")
        .renderHorizontalGridLines(true)
        .transitionDuration(700);

  barChart1.xAxis().tickFormat(function (v) {
    if (v == -1 || v == maxIndex + 1) {
      return "";
    }
    return rooms1[v];
  });

  //Bubble Chart
  bubbleChart = dc.bubbleChart("#bubbleChartJ")

  bubbleChart.width(800 * 0.8)
          .height(350 * 0.8)
          .margins({top: 10, right: 50, bottom: 30, left: 40})
          .transitionDuration(700)
          .dimension(rooms)
          .group(kW)
          .x(d3.scale.linear().domain([-1, maxIndex + 1]))
          .y(d3.scale.linear().domain([-100, 3000]))
          .radiusValueAccessor(function(p) { return p.value; })
          .r(d3.scale.linear().domain([0, 10000]))
          .label(function(d) { return "" })
          .yAxisLabel("kW")
          .xAxisLabel("Room");

  bubbleChart.xAxis().tickFormat(function (v) {
    if (v == -1 || v == maxIndex + 1) {
      return "";
    }
    return rooms1[v];
  });

  //RowChart
  rowChart1 = dc.rowChart("#rowChartJ"); //ADD AN AXIS LABEL

  rowChart1.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(rooms)
        .group(kW)
        .elasticX(true)
        .gap(1)
        .x(d3.scale.linear().domain([-1, maxIndex + 1]))
        .label(function(d) { return rooms1[d.key] })
        .transitionDuration(700);

/*
  THIS IS FOR COST
*/
  //Second Bar Chart
  barChart12 = dc.barChart("#barChart12J");
  
  barChart12.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(rooms2)
        .group(cost)
        .elasticY(true)
        .centerBar(true)
        .gap(1)
        .x(d3.scale.linear().domain([-1, maxIndex + 1]))
        .yAxisLabel("US Dollars")
        .xAxisLabel("Room")
        .renderHorizontalGridLines(true)
        .transitionDuration(700);

  barChart12.xAxis().tickFormat(function (v) {
    if (v == -1 || v == maxIndex + 1) {
      return "";
    }
    return rooms1[v];
  });

  //Bubble Chart
  bubbleChart2 = dc.bubbleChart("#bubbleChart2J")

  bubbleChart2.width(800 * 0.8)
          .height(350 * 0.8)
          .margins({top: 10, right: 50, bottom: 30, left: 40})
          .transitionDuration(700)
          .dimension(rooms2)
          .group(cost)
          .x(d3.scale.linear().domain([-1, maxIndex + 1]))
          .y(d3.scale.linear().domain([-50, 600]))
          .radiusValueAccessor(function(p) { return p.value; })
          .r(d3.scale.linear().domain([0, 10000]))
          .label(function(d) { return "" })
          .yAxisLabel("US Dollars")
          .xAxisLabel("Room");

  bubbleChart2.xAxis().tickFormat(function (v) {
    if (v == -1 || v == maxIndex + 1) {
      return "";
    }
    return rooms1[v];
  });

  //RowChart
  rowChart12 = dc.rowChart("#rowChart2J"); //ADD AN AXIS LABEL

  rowChart12.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(rooms2)
        .group(cost)
        .elasticX(true)
        .gap(1)
        .x(d3.scale.linear().domain([-1, maxIndex + 1]))
        .label(function(d) { return rooms1[d.key] })
        .transitionDuration(700);

  dc.renderAll();
});

























/* 


FEBRUARY


*/
d3.json("/location/data?location_uuid=8639f0e6-1623-4e60-8164-a853fb807917&q_string=fromTime%3D2015-02-01%2005:00:00Z%26toTime%3D2015-03-01%2004:00:00Z%26interval%3Dhour%26aggregate%3dfalse%26data_format%3Drickshaw", function(error, rawData) {
  //Totals
  var totals = {},
      costs = {},
      maxTotal = 0,
      forGraph = [];

  rawData.forEach(function(d) {
    var name = d.sublocation_name,
        data = d.data;

    var total = 0;
    var totalC = 0;
    for (var k = 0; k < data.length; k++) {
      total += data[k].y;
      totalC += data[k].total_cost;
    }

    if (Object.keys(totals).indexOf(name) != -1) {
      totals[name] += total;
      if (totals[name] > maxTotal) {
        maxTotal = totals[name];
      }
    } else {
      totals[name] = total;
      if (totals[name] > maxTotal) {
        maxTotal = totals[name];
      }
    }

    if (Object.keys(costs).indexOf("" + name) != -1) {
      costs[name] += totalC;
    } else {
      costs[name] = totalC;
    }
  });

  var maxIndex = 0;

  var arr = Object.keys(totals);
  for (var i = 0; i < arr.length; i++) {
    maxIndex = arr.length - 1;

    forGraph.push({ name: arr[i],
                    index: i,
                    value: totals[arr[i]],
                    cost: costs[arr[i]]});   
  }

  //HTML Output
  var totalCostA = 0;
  var totalKwA = 0;
  for (var z = 0; z < forGraph.length; z++) {
    totalCostA += forGraph[z].cost;
    totalKwA += forGraph[z].value;
  }
  febTotalCost = totalCostA;
  febTotalKw = totalKwA;

  document.getElementById("totalCostFeb").innerHTML = totalCostA.toFixed(2);

  var monthlyCost = "";
  for (var z = 0; z < forGraph.length; z++) {
    monthlyCost = forGraph[z].name + ": $" + forGraph[z].cost.toFixed(2) +
      "<span> (" + (forGraph[z].cost/totalCostA * 100).toFixed(2) + "% of total cost)</span>";
    d3.select("#monthlyCostFeb")
      .append("div")
      .attr("id", "h4div")
      .html(monthlyCost);
  }

  document.getElementById("totalKwFeb").innerHTML = totalKwA.toFixed(2);

  var monthlyKw = "";
  for (var z = 0; z < forGraph.length; z++) {
    monthlyKw = forGraph[z].name + ": " + forGraph[z].value.toFixed(2) + " kW" +
    "<span> (" + (forGraph[z].value/totalKwA * 100).toFixed(2) + "% of total usage)</span>";
    d3.select("#monthlyKwFeb")
      .append("div")
      .attr("id", "h4div")
      .html(monthlyKw);
  }










  var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";

  var date = [];

  //Dates
  rawData.forEach(function(d) {
    d.data.forEach(function (d, i) {
      var time = d.x;
      var d_date = new Date(0);
      d_date.setUTCSeconds(time);
      d.x = d_date.getDate();
      d.month = month[d_date.getMonth()];
    });
  });

  var dateTotal = {},
      dateCost = {},
      months = {},
      max = 0,
      minDate = rawData[0].data[0].x;
      maxDate = rawData[rawData.length - 1].data[rawData[rawData.length - 1].data.length - 1].x;
      forChart = [];

  rawData.forEach(function(d) {
    var name1 = d.sublocation_name

    d.data.forEach(function (d, i) {
      var name = d.x, //Change to date
          output = d.y;
          month = d.month;
          cost = d.total_cost;

      if (name < minDate) {
        minDate = name;
      }
      if (name > maxDate) {
        maxDate = name;
      }

      if (Object.keys(dateTotal).indexOf("" + name) != -1) {
        dateTotal[name] += output;
        if (dateTotal[name] > max) {
          max = dateTotal[name];
        }
      } else {
        dateTotal[name] = output;
        months[name] = month;
        if (dateTotal[name] > max) {
          max = dateTotal[name];
        }
      }
      if (Object.keys(dateCost).indexOf("" + name) != -1) {
        dateCost[name] += cost;
      } else {
        dateCost[name] = cost;
      }
    });
  });

  var arr = Object.keys(dateTotal);
  for (var i = 0; i < arr.length; i++) {
    forChart.push({ date: arr[i],
                    month: months[arr[i]],
                    value: dateTotal[arr[i]],
                    cost: dateCost[arr[i]]
                  });    
  }

  //Get weekly data
  var weekly = [];
  var weekRanges = [];

  for (var a = 0; a < forChart.length / 7; a++) {
    var name = "week " + a;
    weekly[a] = [];
    var minR = forChart[a * 7].month + " " + forChart[a * 7].date;
    var maxR;

    for (var b = 0; b < forChart.length; b++) {
      if (b == (a + 1) * 7 - 1 || a > 3 && b == forChart.length - 1) {
        weekRanges.push({ range: minR + " - " + forChart[b].date });
      }
      if (b >= (a * 7) && b < (a + 1) * 7) {
        weekly[a].push({ date: forChart[b].date,
                         value: forChart[b].value
                       });
      }
    }
  }




  //Add the monthly data
  monthData.push({month: forChart[0].month,
                  data: forChart});



  //Crossfilters
  var data = forChart;
  var data1 = forGraph;

  var ndx = crossfilter(data);
  var ndx1 = crossfilter(data1);

  var dates = ndx.dimension(function(d) {
    return parseInt(d.date);
  });

  var kW = dates.group().reduceSum(function(d) {
    return d.value;
  });

  var cost = dates.group().reduceSum(function(d) {
    return d.cost;
  });

  var dates1  = ndx.dimension(function(d, i) {
    return parseInt(d.date);
  });

  var kW1 = dates1.group().reduceSum(function(d) {
    return d.value;
  });

  //Bar Graph
  barChartF = dc.barChart("#barChartF");
  
  barChartF.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(dates)
        .group(kW)
        .elasticY(true)
        .centerBar(true)
        .gap(1)
        .x(d3.scale.linear().domain([minDate - 1, maxDate + 1]))
        .yAxisLabel("kW")
        .xAxisLabel("Date")
        .renderHorizontalGridLines(true)
        .transitionDuration(700);

  barChartF.xAxis().tickFormat(function (v) {
    if (v == minDate - 1 || v == maxDate + 1) {
      return "";
    }
    return data[v].month + " " + v;
  });

  //Line Graph
  lineChartF = dc.lineChart("#lineChartF");

  lineChartF.width(800 * 0.8)
    .height(350 * 0.8)
    .margins({top: 10, right: 50, bottom: 30, left: 40})
    .dimension(dates)
    .group(cost, "cost")
    .x(d3.scale.linear().domain([minDate, maxDate]))
    .legend(dc.legend().x(60).y(10).itemHeight(12).gap(5))
    .transitionDuration(700)
    .yAxisLabel("US Dollars")
    .xAxisLabel("Date");

  lineChartF.xAxis().ticks(7);

  lineChartF.xAxis().tickFormat(function (v) {
    if (v == minDate - 1 || v == maxDate + 1) {
      return "";
    }
    return data[v].month + " " + v;
  });

  // //Pie Chart
  // pieChart = dc.pieChart("#pieChart"); //CHANGE TO A BUBBLE CHART
  
  // pieChart.width(800 * 0.8)
  //     .height(400 * 0.8)
  //     .dimension(datesWeek1)
  //     .group(kwWeek1)
  //     .label(function (d) {
  //       return "May " + d.key;
  //     })
  //     .transitionDuration(700);

  bubbleChart1F = dc.bubbleChart("#pieChartF")

  bubbleChart1F.width(800 * 1.6)
          .height(350 * 1)
          .margins({top: 10, right: 50, bottom: 30, left: 40})
          .transitionDuration(700)
          .dimension(dates1)
          .group(kW1)
          .x(d3.scale.linear().domain([minDate - 1, maxDate + 1]))
          .y(d3.scale.linear().domain([0, 450]))
          .radiusValueAccessor(function(p) { return p.value; })
          .r(d3.scale.linear().domain([0, 10000]))
          .label(function(d) { return "" })
          .yAxisLabel("kW")
          .xAxisLabel("Date")

  bubbleChart1F.xAxis().tickFormat(function (v) {
    if (v == minDate - 1 || v == maxDate + 1) {
      return "";
    }
    return data[v].month + " " + v;
  });



  var rooms = ndx1.dimension(function(d) {
    return d.index;
  });

  var kW = rooms.group().reduceSum(function(d) {
    return d.value
  });

  var rooms2 = ndx1.dimension(function(d) {
    return d.index;
  });

  var cost = rooms2.group().reduceSum(function(d) {
    return d.cost
  });

  var rooms1 = {}
  for (var i = 0; i < data1.length; i++) {
    rooms1[i] = data1[i].name;
  }

/*
  THIS IS FOR KW
*/
  //Second Bar Chart
  barChart1 = dc.barChart("#barChart1F");
  
  barChart1.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(rooms)
        .group(kW)
        .elasticY(true)
        .centerBar(true)
        .gap(1)
        .x(d3.scale.linear().domain([-1, maxIndex + 1]))
        .yAxisLabel("kW")
        .xAxisLabel("Room")
        .renderHorizontalGridLines(true)
        .transitionDuration(700);

  barChart1.xAxis().tickFormat(function (v) {
    if (v == -1 || v == maxIndex + 1) {
      return "";
    }
    return rooms1[v];
  });

  //Bubble Chart
  bubbleChart = dc.bubbleChart("#bubbleChartF")

  bubbleChart.width(800 * 0.8)
          .height(350 * 0.8)
          .margins({top: 10, right: 50, bottom: 30, left: 40})
          .transitionDuration(700)
          .dimension(rooms)
          .group(kW)
          .x(d3.scale.linear().domain([-1, maxIndex + 1]))
          .y(d3.scale.linear().domain([-100, 3000]))
          .radiusValueAccessor(function(p) { return p.value; })
          .r(d3.scale.linear().domain([0, 10000]))
          .label(function(d) { return "" })
          .yAxisLabel("kW")
          .xAxisLabel("Room");

  bubbleChart.xAxis().tickFormat(function (v) {
    if (v == -1 || v == maxIndex + 1) {
      return "";
    }
    return rooms1[v];
  });

  //RowChart
  rowChart1 = dc.rowChart("#rowChartF"); //ADD AN AXIS LABEL

  rowChart1.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(rooms)
        .group(kW)
        .elasticX(true)
        .gap(1)
        .x(d3.scale.linear().domain([-1, maxIndex + 1]))
        .label(function(d) { return rooms1[d.key] })
        .transitionDuration(700);

/*
  THIS IS FOR COST
*/
  //Second Bar Chart
  barChart12 = dc.barChart("#barChart12F");
  
  barChart12.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(rooms2)
        .group(cost)
        .elasticY(true)
        .centerBar(true)
        .gap(1)
        .x(d3.scale.linear().domain([-1, maxIndex + 1]))
        .yAxisLabel("US Dollars")
        .xAxisLabel("Room")
        .renderHorizontalGridLines(true)
        .transitionDuration(700);

  barChart12.xAxis().tickFormat(function (v) {
    if (v == -1 || v == maxIndex + 1) {
      return "";
    }
    return rooms1[v];
  });

  //Bubble Chart
  bubbleChart2 = dc.bubbleChart("#bubbleChart2F")

  bubbleChart2.width(800 * 0.8)
          .height(350 * 0.8)
          .margins({top: 10, right: 50, bottom: 30, left: 40})
          .transitionDuration(700)
          .dimension(rooms2)
          .group(cost)
          .x(d3.scale.linear().domain([-1, maxIndex + 1]))
          .y(d3.scale.linear().domain([-50, 600]))
          .radiusValueAccessor(function(p) { return p.value; })
          .r(d3.scale.linear().domain([0, 10000]))
          .label(function(d) { return "" })
          .yAxisLabel("US Dollars")
          .xAxisLabel("Room");

  bubbleChart2.xAxis().tickFormat(function (v) {
    if (v == -1 || v == maxIndex + 1) {
      return "";
    }
    return rooms1[v];
  });

  //RowChart
  rowChart12 = dc.rowChart("#rowChart2F"); //ADD AN AXIS LABEL

  rowChart12.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(rooms2)
        .group(cost)
        .elasticX(true)
        .gap(1)
        .x(d3.scale.linear().domain([-1, maxIndex + 1]))
        .label(function(d) { return rooms1[d.key] })
        .transitionDuration(700);

  dc.renderAll();
});









































/* 


MARCH


*/
d3.json("/location/data?location_uuid=8639f0e6-1623-4e60-8164-a853fb807917&q_string=fromTime%3D2015-03-01%2005:00:00Z%26toTime%3D2015-04-01%2004:00:00Z%26interval%3Dhour%26aggregate%3dfalse%26data_format%3Drickshaw", function(error, rawData) {
  //Totals
  var totals = {},
      costs = {},
      maxTotal = 0,
      forGraph = [];

  rawData.forEach(function(d) {
    var name = d.sublocation_name,
        data = d.data;

    var total = 0;
    var totalC = 0;
    for (var k = 0; k < data.length; k++) {
      total += data[k].y;
      totalC += data[k].total_cost;
    }

    if (Object.keys(totals).indexOf(name) != -1) {
      totals[name] += total;
      if (totals[name] > maxTotal) {
        maxTotal = totals[name];
      }
    } else {
      totals[name] = total;
      if (totals[name] > maxTotal) {
        maxTotal = totals[name];
      }
    }

    if (Object.keys(costs).indexOf("" + name) != -1) {
      costs[name] += totalC;
    } else {
      costs[name] = totalC;
    }
  });

  var maxIndex = 0;

  var arr = Object.keys(totals);
  for (var i = 0; i < arr.length; i++) {
    maxIndex = arr.length - 1;

    forGraph.push({ name: arr[i],
                    index: i,
                    value: totals[arr[i]],
                    cost: costs[arr[i]]});   
  }

  //HTML Output
  var totalCostA = 0;
  var totalKwA = 0;
  for (var z = 0; z < forGraph.length; z++) {
    totalCostA += forGraph[z].cost;
    totalKwA += forGraph[z].value;
  }
  marTotalCost = totalCostA;
  marTotalKw = totalKwA;

  document.getElementById("totalCostMar").innerHTML = totalCostA.toFixed(2);

  var monthlyCost = "";
  for (var z = 0; z < forGraph.length; z++) {
    monthlyCost = forGraph[z].name + ": $" + forGraph[z].cost.toFixed(2) +
      "<span> (" + (forGraph[z].cost/totalCostA * 100).toFixed(2) + "% of total cost)</span>";
    d3.select("#monthlyCostMar")
      .append("div")
      .attr("id", "h4div")
      .html(monthlyCost);
  }

  document.getElementById("totalKwMar").innerHTML = totalKwA.toFixed(2);

  var monthlyKw = "";
  for (var z = 0; z < forGraph.length; z++) {
    monthlyKw = forGraph[z].name + ": " + forGraph[z].value.toFixed(2) + " kW" +
    "<span> (" + (forGraph[z].value/totalKwA * 100).toFixed(2) + "% of total usage)</span>";
    d3.select("#monthlyKwMar")
      .append("div")
      .attr("id", "h4div")
      .html(monthlyKw);
  }













  var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";

  var date = [];

  //Dates
  rawData.forEach(function(d) {
    d.data.forEach(function (d, i) {
      var time = d.x;
      var d_date = new Date(0);
      d_date.setUTCSeconds(time);
      d.x = d_date.getDate();
      d.month = month[d_date.getMonth()];
    });
  });

  var dateTotal = {},
      dateCost = {},
      months = {},
      max = 0,
      minDate = rawData[0].data[0].x;
      maxDate = rawData[rawData.length - 1].data[rawData[rawData.length - 1].data.length - 1].x;
      forChart = [];

  rawData.forEach(function(d) {
    var name1 = d.sublocation_name

    d.data.forEach(function (d, i) {
      var name = d.x, //Change to date
          output = d.y;
          month = d.month;
          cost = d.total_cost;

      if (name < minDate) {
        minDate = name;
      }
      if (name > maxDate) {
        maxDate = name;
      }

      if (Object.keys(dateTotal).indexOf("" + name) != -1) {
        dateTotal[name] += output;
        if (dateTotal[name] > max) {
          max = dateTotal[name];
        }
      } else {
        dateTotal[name] = output;
        months[name] = month;
        if (dateTotal[name] > max) {
          max = dateTotal[name];
        }
      }
      if (Object.keys(dateCost).indexOf("" + name) != -1) {
        dateCost[name] += cost;
      } else {
        dateCost[name] = cost;
      }
    });
  });

  var arr = Object.keys(dateTotal);
  for (var i = 0; i < arr.length; i++) {
    forChart.push({ date: arr[i],
                    month: months[arr[i]],
                    value: dateTotal[arr[i]],
                    cost: dateCost[arr[i]]
                  });    
  }

  //Get weekly data
  var weekly = [];
  var weekRanges = [];

  for (var a = 0; a < forChart.length / 7; a++) {
    var name = "week " + a;
    weekly[a] = [];
    var minR = forChart[a * 7].month + " " + forChart[a * 7].date;
    var maxR;

    for (var b = 0; b < forChart.length; b++) {
      if (b == (a + 1) * 7 - 1 || a > 3 && b == forChart.length - 1) {
        weekRanges.push({ range: minR + " - " + forChart[b].date });
      }
      if (b >= (a * 7) && b < (a + 1) * 7) {
        weekly[a].push({ date: forChart[b].date,
                         value: forChart[b].value
                       });
      }
    }
  }





  //Add the monthly data
  monthData.push({month: forChart[0].month,
                  data: forChart});


  //Crossfilters
  var data = forChart;
  var data1 = forGraph;

  var ndx = crossfilter(data);
  var ndx1 = crossfilter(data1);

  var dates = ndx.dimension(function(d) {
    return parseInt(d.date);
  });

  var kW = dates.group().reduceSum(function(d) {
    return d.value;
  });

  var cost = dates.group().reduceSum(function(d) {
    return d.cost;
  });

  var dates1  = ndx.dimension(function(d, i) {
    return parseInt(d.date);
  });

  var kW1 = dates1.group().reduceSum(function(d) {
    return d.value;
  });

  //Bar Graph
  barChartM = dc.barChart("#barChartM");
  
  barChartM.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(dates)
        .group(kW)
        .elasticY(true)
        .centerBar(true)
        .gap(1)
        .x(d3.scale.linear().domain([minDate - 1, maxDate + 1]))
        .yAxisLabel("kW")
        .xAxisLabel("Date")
        .renderHorizontalGridLines(true)
        .transitionDuration(700);

  barChartM.xAxis().tickFormat(function (v) {
    if (v == minDate - 1 || v == maxDate + 1) {
      return "";
    }
    return data[v].month + " " + v;
  });

  //Line Graph
  lineChartM = dc.lineChart("#lineChartM");

  lineChartM.width(800 * 0.8)
    .height(350 * 0.8)
    .margins({top: 10, right: 50, bottom: 30, left: 40})
    .dimension(dates)
    .group(cost, "cost")
    .x(d3.scale.linear().domain([minDate, maxDate]))
    .legend(dc.legend().x(60).y(10).itemHeight(12).gap(5))
    .transitionDuration(700)
    .yAxisLabel("US Dollars")
    .xAxisLabel("Date");

  lineChartM.xAxis().ticks(7);

  lineChartM.xAxis().tickFormat(function (v) {
    if (v == minDate - 1 || v == maxDate + 1) {
      return "";
    }
    return data[v].month + " " + v;
  });

  // //Pie Chart
  // pieChart = dc.pieChart("#pieChart"); //CHANGE TO A BUBBLE CHART
  
  // pieChart.width(800 * 0.8)
  //     .height(400 * 0.8)
  //     .dimension(datesWeek1)
  //     .group(kwWeek1)
  //     .label(function (d) {
  //       return "May " + d.key;
  //     })
  //     .transitionDuration(700);

  bubbleChart1M = dc.bubbleChart("#pieChartM")

  bubbleChart1M.width(800 * 1.6)
          .height(350 * 1)
          .margins({top: 10, right: 50, bottom: 30, left: 40})
          .transitionDuration(700)
          .dimension(dates1)
          .group(kW1)
          .x(d3.scale.linear().domain([minDate - 1, maxDate + 1]))
          .y(d3.scale.linear().domain([0, 450]))
          .radiusValueAccessor(function(p) { return p.value; })
          .r(d3.scale.linear().domain([0, 10000]))
          .label(function(d) { return "" })
          .yAxisLabel("kW")
          .xAxisLabel("Date")

  bubbleChart1M.xAxis().tickFormat(function (v) {
    if (v == minDate - 1 || v == maxDate + 1) {
      return "";
    }
    return data[v].month + " " + v;
  });



  var rooms = ndx1.dimension(function(d) {
    return d.index;
  });

  var kW = rooms.group().reduceSum(function(d) {
    return d.value
  });

  var rooms2 = ndx1.dimension(function(d) {
    return d.index;
  });

  var cost = rooms2.group().reduceSum(function(d) {
    return d.cost
  });

  var rooms1 = {}
  for (var i = 0; i < data1.length; i++) {
    rooms1[i] = data1[i].name;
  }

/*
  THIS IS FOR KW
*/
  //Second Bar Chart
  barChart1 = dc.barChart("#barChart1M");
  
  barChart1.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(rooms)
        .group(kW)
        .elasticY(true)
        .centerBar(true)
        .gap(1)
        .x(d3.scale.linear().domain([-1, maxIndex + 1]))
        .yAxisLabel("kW")
        .xAxisLabel("Room")
        .renderHorizontalGridLines(true)
        .transitionDuration(700);

  barChart1.xAxis().tickFormat(function (v) {
    if (v == -1 || v == maxIndex + 1) {
      return "";
    }
    return rooms1[v];
  });

  //Bubble Chart
  bubbleChart = dc.bubbleChart("#bubbleChartM")

  bubbleChart.width(800 * 0.8)
          .height(350 * 0.8)
          .margins({top: 10, right: 50, bottom: 30, left: 40})
          .transitionDuration(700)
          .dimension(rooms)
          .group(kW)
          .x(d3.scale.linear().domain([-1, maxIndex + 1]))
          .y(d3.scale.linear().domain([-100, 3000]))
          .radiusValueAccessor(function(p) { return p.value; })
          .r(d3.scale.linear().domain([0, 10000]))
          .label(function(d) { return "" })
          .yAxisLabel("kW")
          .xAxisLabel("Room");

  bubbleChart.xAxis().tickFormat(function (v) {
    if (v == -1 || v == maxIndex + 1) {
      return "";
    }
    return rooms1[v];
  });

  //RowChart
  rowChart1 = dc.rowChart("#rowChartM"); //ADD AN AXIS LABEL

  rowChart1.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(rooms)
        .group(kW)
        .elasticX(true)
        .gap(1)
        .x(d3.scale.linear().domain([-1, maxIndex + 1]))
        .label(function(d) { return rooms1[d.key] })
        .transitionDuration(700);

/*
  THIS IS FOR COST
*/
  //Second Bar Chart
  barChart12 = dc.barChart("#barChart12M");
  
  barChart12.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(rooms2)
        .group(cost)
        .elasticY(true)
        .centerBar(true)
        .gap(1)
        .x(d3.scale.linear().domain([-1, maxIndex + 1]))
        .yAxisLabel("US Dollars")
        .xAxisLabel("Room")
        .renderHorizontalGridLines(true)
        .transitionDuration(700);

  barChart12.xAxis().tickFormat(function (v) {
    if (v == -1 || v == maxIndex + 1) {
      return "";
    }
    return rooms1[v];
  });

  //Bubble Chart
  bubbleChart2 = dc.bubbleChart("#bubbleChart2M")

  bubbleChart2.width(800 * 0.8)
          .height(350 * 0.8)
          .margins({top: 10, right: 50, bottom: 30, left: 40})
          .transitionDuration(700)
          .dimension(rooms2)
          .group(cost)
          .x(d3.scale.linear().domain([-1, maxIndex + 1]))
          .y(d3.scale.linear().domain([-50, 600]))
          .radiusValueAccessor(function(p) { return p.value; })
          .r(d3.scale.linear().domain([0, 10000]))
          .label(function(d) { return "" })
          .yAxisLabel("US Dollars")
          .xAxisLabel("Room");

  bubbleChart2.xAxis().tickFormat(function (v) {
    if (v == -1 || v == maxIndex + 1) {
      return "";
    }
    return rooms1[v];
  });

  //RowChart
  rowChart12 = dc.rowChart("#rowChart2M"); //ADD AN AXIS LABEL

  rowChart12.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(rooms2)
        .group(cost)
        .elasticX(true)
        .gap(1)
        .x(d3.scale.linear().domain([-1, maxIndex + 1]))
        .label(function(d) { return rooms1[d.key] })
        .transitionDuration(700);

  dc.renderAll();
});



















/* 


APRIL


*/
d3.json("/location/data?location_uuid=8639f0e6-1623-4e60-8164-a853fb807917&q_string=fromTime%3D2015-04-01%2005:00:00Z%26toTime%3D2015-05-1%2004:00:00Z%26interval%3Dhour%26aggregate%3dfalse%26data_format%3Drickshaw", function(error, rawData) {
  //Totals
  var totals = {},
      costs = {},
      maxTotal = 0,
      forGraph = [];

  rawData.forEach(function(d) {
    var name = d.sublocation_name,
        data = d.data;

    var total = 0;
    var totalC = 0;
    for (var k = 0; k < data.length; k++) {
      total += data[k].y;
      totalC += data[k].total_cost;
    }

    if (Object.keys(totals).indexOf(name) != -1) {
      totals[name] += total;
      if (totals[name] > maxTotal) {
        maxTotal = totals[name];
      }
    } else {
      totals[name] = total;
      if (totals[name] > maxTotal) {
        maxTotal = totals[name];
      }
    }

    if (Object.keys(costs).indexOf("" + name) != -1) {
      costs[name] += totalC;
    } else {
      costs[name] = totalC;
    }
  });

  var maxIndex = 0;

  var arr = Object.keys(totals);
  for (var i = 0; i < arr.length; i++) {
    maxIndex = arr.length - 1;

    forGraph.push({ name: arr[i],
                    index: i,
                    value: totals[arr[i]],
                    cost: costs[arr[i]]});   
  }

  //HTML Output
  var totalCostA = 0;
  var totalKwA = 0;
  for (var z = 0; z < forGraph.length; z++) {
    totalCostA += forGraph[z].cost;
    totalKwA += forGraph[z].value;
  }
  aprTotalCost = totalCostA;
  aprTotalKw = totalKwA;

  document.getElementById("totalCostApr").innerHTML = totalCostA.toFixed(2);

  var monthlyCost = "";
  for (var z = 0; z < forGraph.length; z++) {
    monthlyCost = forGraph[z].name + ": $" + forGraph[z].cost.toFixed(2) +
      "<span> (" + (forGraph[z].cost/totalCostA * 100).toFixed(2) + "% of total cost)</span>";
    d3.select("#monthlyCostApr")
      .append("div")
      .attr("id", "h4div")
      .html(monthlyCost);
  }

  document.getElementById("totalKwApr").innerHTML = totalKwA.toFixed(2);

  var monthlyKw = "";
  for (var z = 0; z < forGraph.length; z++) {
    monthlyKw = forGraph[z].name + ": " + forGraph[z].value.toFixed(2) + " kW" +
    "<span> (" + (forGraph[z].value/totalKwA * 100).toFixed(2) + "% of total usage)</span>";
    d3.select("#monthlyKwApr")
      .append("div")
      .attr("id", "h4div")
      .html(monthlyKw);
  }










  var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";

  var date = [];

  //Dates
  rawData.forEach(function(d) {
    d.data.forEach(function (d, i) {
      var time = d.x;
      var d_date = new Date(0);
      d_date.setUTCSeconds(time);
      d.x = d_date.getDate();
      d.month = month[d_date.getMonth()];
    });
  });

  var dateTotal = {},
      dateCost = {},
      months = {},
      max = 0,
      minDate = rawData[0].data[0].x;
      maxDate = rawData[rawData.length - 1].data[rawData[rawData.length - 1].data.length - 1].x;
      forChart = [];

  rawData.forEach(function(d) {
    var name1 = d.sublocation_name

    d.data.forEach(function (d, i) {
      var name = d.x, //Change to date
          output = d.y;
          month = d.month;
          cost = d.total_cost;

      if (name < minDate) {
        minDate = name;
      }
      if (name > maxDate) {
        maxDate = name;
      }

      if (Object.keys(dateTotal).indexOf("" + name) != -1) {
        dateTotal[name] += output;
        if (dateTotal[name] > max) {
          max = dateTotal[name];
        }
      } else {
        dateTotal[name] = output;
        months[name] = month;
        if (dateTotal[name] > max) {
          max = dateTotal[name];
        }
      }
      if (Object.keys(dateCost).indexOf("" + name) != -1) {
        dateCost[name] += cost;
      } else {
        dateCost[name] = cost;
      }
    });
  });

  var arr = Object.keys(dateTotal);
  for (var i = 0; i < arr.length; i++) {
    forChart.push({ date: arr[i],
                    month: months[arr[i]],
                    value: dateTotal[arr[i]],
                    cost: dateCost[arr[i]]
                  });    
  }

  //Get weekly data
  var weekly = [];
  var weekRanges = [];

  for (var a = 0; a < forChart.length / 7; a++) {
    var name = "week " + a;
    weekly[a] = [];
    var minR = forChart[a * 7].month + " " + forChart[a * 7].date;
    var maxR;

    for (var b = 0; b < forChart.length; b++) {
      if (b == (a + 1) * 7 - 1 || a > 3 && b == forChart.length - 1) {
        weekRanges.push({ range: minR + " - " + forChart[b].date });
      }
      if (b >= (a * 7) && b < (a + 1) * 7) {
        weekly[a].push({ date: forChart[b].date,
                         value: forChart[b].value
                       });
      }
    }
  }




  //Add the monthly data
  monthData.push({month: forChart[0].month,
                  data: forChart});

  //Crossfilters
  var data = forChart;
  var data1 = forGraph;

  var ndx = crossfilter(data);
  var ndx1 = crossfilter(data1);

  var dates = ndx.dimension(function(d) {
    return parseInt(d.date);
  });

  var kW = dates.group().reduceSum(function(d) {
    return d.value;
  });

  var cost = dates.group().reduceSum(function(d) {
    return d.cost;
  });

  var dates1  = ndx.dimension(function(d, i) {
    return parseInt(d.date);
  });

  var kW1 = dates1.group().reduceSum(function(d) {
    return d.value;
  });

  //Bar Graph
  barChart = dc.barChart("#barChartA");
  
  barChart.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(dates)
        .group(kW)
        .elasticY(true)
        .centerBar(true)
        .gap(1)
        .x(d3.scale.linear().domain([minDate - 1, maxDate + 1]))
        .yAxisLabel("kW")
        .xAxisLabel("Date")
        .renderHorizontalGridLines(true)
        .transitionDuration(700);

  barChart.xAxis().tickFormat(function (v) {
    if (v == minDate - 1 || v == maxDate + 1) {
      return "";
    }
    if (v > 29) {
      return data[v - 1].month + " " + v;
    } else {
      return data[v].month + " " + v;
    }
  });

  //Line Graph
  lineChart = dc.lineChart("#lineChartA");

  lineChart.width(800 * 0.8)
    .height(350 * 0.8)
    .margins({top: 10, right: 50, bottom: 30, left: 40})
    .dimension(dates)
    .group(cost, "cost")
    .x(d3.scale.linear().domain([minDate, maxDate]))
    .legend(dc.legend().x(60).y(10).itemHeight(12).gap(5))
    .transitionDuration(700)
    .yAxisLabel("US Dollars")
    .xAxisLabel("Date");

  lineChart.xAxis().ticks(7);

  lineChart.xAxis().tickFormat(function (v) {
    if (v == minDate - 1 || v == maxDate + 1) {
      return "";
    }
    if (v > 29) {
      return data[v - 1].month + " " + v;
    } else {
      return data[v].month + " " + v;
    }
  });

  // //Pie Chart
  // pieChart = dc.pieChart("#pieChart"); //CHANGE TO A BUBBLE CHART
  
  // pieChart.width(800 * 0.8)
  //     .height(400 * 0.8)
  //     .dimension(datesWeek1)
  //     .group(kwWeek1)
  //     .label(function (d) {
  //       return "May " + d.key;
  //     })
  //     .transitionDuration(700);

  bubbleChart1 = dc.bubbleChart("#pieChartA")

  bubbleChart1.width(800 * 1.6)
          .height(350 * 1)
          .margins({top: 10, right: 50, bottom: 30, left: 40})
          .transitionDuration(700)
          .dimension(dates1)
          .group(kW1)
          .x(d3.scale.linear().domain([minDate - 1, maxDate + 1]))
          .y(d3.scale.linear().domain([0, 450]))
          .radiusValueAccessor(function(p) { return p.value; })
          .r(d3.scale.linear().domain([0, 10000]))
          .label(function(d) { return "" })
          .yAxisLabel("kW")
          .xAxisLabel("Date")

  bubbleChart1.xAxis().tickFormat(function (v) {
    if (v == minDate - 1 || v == maxDate + 1) {
      return "";
    }
    if (v > 29) {
      return data[v - 1].month + " " + v;
    } else {
      return data[v].month + " " + v;
    }
  });



  var rooms = ndx1.dimension(function(d) {
    return d.index;
  });

  var kW = rooms.group().reduceSum(function(d) {
    return d.value
  });

  var rooms2 = ndx1.dimension(function(d) {
    return d.index;
  });

  var cost = rooms2.group().reduceSum(function(d) {
    return d.cost
  });

  var rooms1 = {}
  for (var i = 0; i < data1.length; i++) {
    rooms1[i] = data1[i].name;
  }

/*
  THIS IS FOR KW
*/
  //Second Bar Chart
  barChart1 = dc.barChart("#barChart1A");
  
  barChart1.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(rooms)
        .group(kW)
        .elasticY(true)
        .centerBar(true)
        .gap(1)
        .x(d3.scale.linear().domain([-1, maxIndex + 1]))
        .yAxisLabel("kW")
        .xAxisLabel("Room")
        .renderHorizontalGridLines(true)
        .transitionDuration(700);

  barChart1.xAxis().tickFormat(function (v) {
    if (v == -1 || v == maxIndex + 1) {
      return "";
    }
    return rooms1[v];
  });

  //Bubble Chart
  bubbleChart = dc.bubbleChart("#bubbleChartA")

  bubbleChart.width(800 * 0.8)
          .height(350 * 0.8)
          .margins({top: 10, right: 50, bottom: 30, left: 40})
          .transitionDuration(700)
          .dimension(rooms)
          .group(kW)
          .x(d3.scale.linear().domain([-1, maxIndex + 1]))
          .y(d3.scale.linear().domain([-100, 3000]))
          .radiusValueAccessor(function(p) { return p.value; })
          .r(d3.scale.linear().domain([0, 10000]))
          .label(function(d) { return "" })
          .yAxisLabel("kW")
          .xAxisLabel("Room");

  bubbleChart.xAxis().tickFormat(function (v) {
    if (v == -1 || v == maxIndex + 1) {
      return "";
    }
    return rooms1[v];
  });

  //RowChart
  rowChart1 = dc.rowChart("#rowChartA"); //ADD AN AXIS LABEL

  rowChart1.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(rooms)
        .group(kW)
        .elasticX(true)
        .gap(1)
        .x(d3.scale.linear().domain([-1, maxIndex + 1]))
        .label(function(d) { return rooms1[d.key] })
        .transitionDuration(700);

/*
  THIS IS FOR COST
*/
  //Second Bar Chart
  barChart12 = dc.barChart("#barChart12A");
  
  barChart12.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(rooms2)
        .group(cost)
        .elasticY(true)
        .centerBar(true)
        .gap(1)
        .x(d3.scale.linear().domain([-1, maxIndex + 1]))
        .yAxisLabel("US Dollars")
        .xAxisLabel("Room")
        .renderHorizontalGridLines(true)
        .transitionDuration(700);

  barChart12.xAxis().tickFormat(function (v) {
    if (v == -1 || v == maxIndex + 1) {
      return "";
    }
    return rooms1[v];
  });

  //Bubble Chart
  bubbleChart2 = dc.bubbleChart("#bubbleChart2A")

  bubbleChart2.width(800 * 0.8)
          .height(350 * 0.8)
          .margins({top: 10, right: 50, bottom: 30, left: 40})
          .transitionDuration(700)
          .dimension(rooms2)
          .group(cost)
          .x(d3.scale.linear().domain([-1, maxIndex + 1]))
          .y(d3.scale.linear().domain([-50, 600]))
          .radiusValueAccessor(function(p) { return p.value; })
          .r(d3.scale.linear().domain([0, 10000]))
          .label(function(d) { return "" })
          .yAxisLabel("US Dollars")
          .xAxisLabel("Room");

  bubbleChart2.xAxis().tickFormat(function (v) {
    if (v == -1 || v == maxIndex + 1) {
      return "";
    }
    return rooms1[v];
  });

  //RowChart
  rowChart12 = dc.rowChart("#rowChart2A"); //ADD AN AXIS LABEL

  rowChart12.width(800 * 0.8)
        .height(350 * 0.8)
        .margins({top: 10, right: 50, bottom: 30, left: 40})
        .dimension(rooms2)
        .group(cost)
        .elasticX(true)
        .gap(1)
        .x(d3.scale.linear().domain([-1, maxIndex + 1]))
        .label(function(d) { return rooms1[d.key] })
        .transitionDuration(700);

  //Comparison calculations
  janFebTotalCost = "$" + (febTotalCost - janTotalCost).toFixed(2);
  nextLineJF = "increase from last month";
  d3.select("#janFebCost")
    .append("li")
    .attr("id", "dollarValue")
    .html(janFebTotalCost);
  d3.select("#janFebCost")
    .append("li")
    .attr("id", "iord")
    .html(nextLineJF);
  janFebTotalKw = (febTotalKw - janTotalKw).toFixed(2) + " kW";
  d3.select("#janFebKw")
    .append("li")
    .attr("id", "dollarValue")
    .html(janFebTotalKw);
  d3.select("#janFebKw")
    .append("li")
    .attr("id", "iord")
    .html(nextLineJF);

  febMarTotalCost = "$" + (marTotalCost - febTotalCost).toFixed(2);
  nextLineFM = "increase from last month";
  d3.select("#febMarCost")
    .append("li")
    .attr("id", "dollarValue")
    .html(febMarTotalCost);
  d3.select("#febMarCost")
    .append("li")
    .attr("id", "iord")
    .html(nextLineFM);
  febMarTotalKw = (marTotalKw - febTotalKw).toFixed(2) + " kW";
  d3.select("#febMarKw")
    .append("li")
    .attr("id", "dollarValue")
    .html(febMarTotalKw);
  d3.select("#febMarKw")
    .append("li")
    .attr("id", "iord")
    .html(nextLineFM);

  if (marAprTotalCost < 0) {
    marAprTotalCost *= -1;
  }
  marAprTotalCost = "$" + ((aprTotalCost - marTotalCost) * -1).toFixed(2);
  nextLineMA = "decrease from last month";
  d3.select("#marAprCost")
    .append("li")
    .attr("id", "dollarValue")
    .html(marAprTotalCost);
  d3.select("#marAprCost")
    .append("li")
    .attr("id", "iord")
    .html(nextLineMA);
  if (marAprTotalKw < 0) {
    marAprTotalKw *= -1;
  }
  marAprTotalKw = ((aprTotalKw - marTotalKw) * -1).toFixed(2) + " kW";
  d3.select("#marAprKw")
    .append("li")
    .attr("id", "dollarValue")
    .html(marAprTotalKw);
  d3.select("#marAprKw")
    .append("li")
    .attr("id", "iord")
    .html(nextLineMA);

  toMayTotalCost = "$" + (mayTotalCost - aprTotalCost).toFixed(2);
  nextLineM = "increase from last month";
  d3.select("#toMayCost")
    .append("li")
    .attr("id", "dollarValue")
    .html(toMayTotalCost);
  d3.select("#toMayCost")
    .append("li")
    .attr("id", "iord")
    .html(nextLineM);
  toMayTotalKw = (mayTotalKw - aprTotalKw).toFixed(2) + " kW";
  d3.select("#toMayKw")
    .append("li")
    .attr("id", "dollarValue")
    .html(toMayTotalKw);
  d3.select("#toMayKw")
    .append("li")
    .attr("id", "iord")
    .html(nextLineM);

  dc.renderAll();


  //Number text
  d3.select("#mayDollars")
    .html("$" + mayTotalCost.toFixed(2));

  d3.select("#janDollars")
    .html("$" + janTotalCost.toFixed(2));

  d3.select("#febDollars")
    .html("$" + febTotalCost.toFixed(2));

  d3.select("#marDollars")
    .html("$" + marTotalCost.toFixed(2));

  d3.select("#aprDollars")
    .html("$" + aprTotalCost.toFixed(2));

  var totalCostForYear = mayTotalCost + janTotalCost + febTotalCost + marTotalCost + aprTotalCost;
  d3.select("#totalCostForYear")
    .html("Total: " + "<span class='totalDollars'>$" + totalCostForYear.toFixed(2) + "</span>");

  //Overview
  var margin = {top: 30, right: 40, bottom: 30, left: 50},
    width = 800 * 1.6 - margin.left - margin.right,
    height = 350 - margin.top - margin.bottom;

  var x = d3.scale.linear().range([0, width]);
  var y = d3.scale.linear().range([height, 0]);

  var xAxis = d3.svg.axis().scale(x)
    .orient("bottom").ticks(5);

  var yAxis = d3.svg.axis().scale(y)
    .orient("left").ticks(5);

  var valuelineMay = d3.svg.line()
    .x(function(d) { return x(parseInt(d.date)); })
    .y(function(d) { return y(d.cost); });
    
  var valuelineJan = d3.svg.line()
    .x(function(d) { return x(parseInt(d.date)); })
    .y(function(d) { return y(d.cost); });

  var valuelineFeb = d3.svg.line()
    .x(function(d) { return x(parseInt(d.date)); })
    .y(function(d) { return y(d.cost); });

  var valuelineMar = d3.svg.line()
    .x(function(d) { return x(parseInt(d.date)); })
    .y(function(d) { return y(d.cost); });

  var valuelineApr = d3.svg.line()
    .x(function(d) { return x(parseInt(d.date)); })
    .y(function(d) { return y(d.cost); });
    
  var svg = d3.select("#monthlyDataCost")
    .append("svg")
      .attr("width", 800 * 1.6)
      .attr("height", 350)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


  //Scale the range of the data
  x.domain(d3.extent(monthData[0].data, function(d) { return parseInt(d.date); }));
  y.domain([0, d3.max(monthData[0].data, function(d) { return Math.max(d.cost); })]);

  //Add the lines
  svg.append("path")
    .attr("class", "line")
    .attr("id", "may")
    .attr("d", valuelineMay(monthData[0].data));

  svg.append("path")
    .attr("class", "line")
    .attr("id", "jan")
    .style("stroke", "red")
    .attr("d", valuelineJan(monthData[1].data));

  svg.append("path")
    .attr("class", "line")
    .attr("id", "feb")
    .style("stroke", "green")
    .attr("d", valuelineFeb(monthData[2].data));

  svg.append("path")
    .attr("class", "line")
    .attr("id", "mar")
    .style("stroke", "orange")
    .attr("d", valuelineMar(monthData[3].data));

  svg.append("path")
    .attr("class", "line")
    .attr("id", "apr")
    .style("stroke", "purple")
    .attr("d", valuelineApr(monthData[4].data));

  //Add the x axis
  svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis)
    .append("text")
    .attr("x", 1160)
    .attr("y", -2)
    .text("Date");

  //Add the y axis
  svg.append("g")
    .attr("class", "y axis")
    .call(yAxis)
    .append("text")
    .attr("x", 2)
    .attr("y", 15)
    .text("$");

  //Add the legend
  svg.append("text")
    .attr("x", 420)
    .attr("y", 0)
    .attr("class", "legend")
    .style("fill", "red")         
    .on("click", function(){
      var active   = jan.active ? false : true,
        newOpacity = active ? 0 : 1;
      d3.select("#jan").style("opacity", newOpacity);
      jan.active = active;
    })
    .text("" + monthData[1].data[1].month);

  svg.append("text")
    .attr("x", 520)
    .attr("y", 0)
    .attr("class", "legend")
    .style("fill", "green")         
    .on("click", function(){
      var active   = feb.active ? false : true,
        newOpacity = active ? 0 : 1;
      d3.select("#feb").style("opacity", newOpacity);
      feb.active = active;
    })
    .text("" + monthData[2].data[2].month);
  
  svg.append("text")
    .attr("x", 640)
    .attr("y", 0)
    .attr("class", "legend")
    .style("fill", "orange")         
    .on("click", function(){
      var active   = mar.active ? false : true,
        newOpacity = active ? 0 : 1;
      d3.select("#mar").style("opacity", newOpacity);
      mar.active = active;
    })
    .text("" + monthData[3].data[3].month);
  
  svg.append("text")
    .attr("x", 740)
    .attr("y", 0)
    .attr("class", "legend")
    .style("fill", "purple")         
    .on("click", function(){
      var active   = apr.active ? false : true,
        newOpacity = active ? 0 : 1;
      d3.select("#apr").style("opacity", newOpacity);
      apr.active = active;
    })
    .text("" + monthData[4].data[4].month);

  svg.append("text")
    .attr("x", 820)
    .attr("y", 0)
    .attr("class", "legend")
    .style("fill", "steelblue")         
    .on("click", function(){
      var active   = may.active ? false : true,
        newOpacity = active ? 0 : 1;
      d3.select("#may").style("opacity", newOpacity);
      may.active = active;
    })
    .text("" + monthData[0].data[0].month);
});

//Input function
function addFields(){
  var costForInput = mayTotalCost + janTotalCost + febTotalCost + marTotalCost + aprTotalCost;
  var number = document.getElementById("member").value;
  var container = document.getElementById("container");
  while (container.hasChildNodes()) {
    container.removeChild(container.lastChild);
  }
  d3.select("#container")
    .html(" New total cost: " + "<span class='totalDollars'>$" + ((1 - (number * 0.01)) * costForInput).toFixed(2) +
      "</span>. This saves you " + "<span class='totalDollars'>$" + (costForInput - ((1 - (number * 0.01)) * costForInput)).toFixed(2) + "</span>");
}
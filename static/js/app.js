function addAxes (svg, xAxis, yAxis, margin, chartWidth, chartHeight, loc, equip, index, date) {
  var axes = svg.append('g')

  //x-axis
  axes.append('g')
    .attr('class', 'x axis')
    .attr('transform', 'translate(0,' + chartHeight + ')')
    .call(xAxis)
    .append('text')
    .attr('x', 1168)
    .attr('y', -2)
    .text('Date');

  //y-axis
  axes.append('g')
    .attr('class', 'y axis')
    .call(yAxis)
    .append('text')
    .attr('x', 2)
    .attr('y', 15)
    .text('kW');

  //Header text
  var header = axes.append('g')
    .append('text')
    .attr('x', 500)
    .attr('y', -8)
    .text(loc[index].loc + ": " + equip[index].equip + " (" + date + ")")
    .attr('font-size', '20px')
    .attr('font-weight', 'bold');
}

function drawPaths (svg, data, x, y) {
  var line = d3.svg.line()
    .x(function (d) { return x(d.x); })
    .y(function (d) { return y(d.y); })
    .interpolate('linear');

  // var area = d3.svg.area()
  //   .interpolate('linear')
  //   .x (function (d) { return x(d.x); })
  //   .y0(function (d) { return y(0); })
  //   .y1(function (d) { return y(d.y); });

  svg.datum(data);

  svg.append('path')
    .attr('class', 'line')
    .attr('d', line)
    .attr('clip-path', 'url(#rect-clip)');

  // svg.append('path')
  //   .attr('class', 'area')
  //   .attr('d', area)
  //   .attr('clip-path', 'url(#rect-clip)');
}

function startTransitions (svg, chartWidth, chartHeight, rectClip, x) {
  rectClip.transition().duration(2000).attr('width', chartWidth);
}

function makeChart (data, loc, equip, index) {
  var svgWidth  = 1280,
      svgHeight = 500,
      margin = { top: 40, right: 20, bottom: 40, left: 60 },
      chartWidth  = svgWidth  - margin.left - margin.right,
      chartHeight = svgHeight - margin.top  - margin.bottom;

  //Define the domain and range
  var x = d3.scale.linear().range([0, chartWidth])
            .domain(d3.extent(data, function (d) {
              return d.x;
            })),
      y = d3.scale.linear().range([chartHeight, 0])
            // .domain([0, d3.max(data, function(d) {
            //   return d.y;
            // })]);
            .domain(d3.extent(data, function (d) {
              return d.y;
            }));

  var xAxis = d3.svg.axis()
                .scale(x)
                .tickFormat(function(f){return moment(f*1000)
                  .format('h:mm a');})
                  // .format('YYYY-MM-DD h:mm:ss');})
                .orient('bottom')
                .ticks(8)
                .innerTickSize(-chartHeight)
                .outerTickSize(0)
                .tickPadding(10);

  //Get the date
  var time = data[0].x;
  var d = new Date(0);
  d.setUTCSeconds(time);

  var month = new Array();
  month[0] = "January";
  month[1] = "February";
  month[2] = "March";
  month[3] = "April";
  month[4] = "May";
  month[5] = "June";
  month[6] = "July";
  month[7] = "August";
  month[8] = "September";
  month[9] = "October";
  month[10] = "November";
  month[11] = "December";
  var date = month[d.getMonth()] + " " + d.getDate();

  var yAxis = d3.svg.axis()
                .scale(y)
                .orient('left')
                .innerTickSize(-chartWidth)
                .outerTickSize(0)
                .tickPadding(10);

  var svg = d3.select('body').append('svg')
    .attr('width',  svgWidth)
    .attr('height', svgHeight)
    .append('g')
    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

  //Make the line slide in
  var rectClip = svg.append('clipPath')
    .attr('id', 'rect-clip')
    .append('rect')
    .attr('width', 0)
    .attr('height', chartHeight);

  drawPaths(svg, data, x, y);
  startTransitions(svg, chartWidth, chartHeight, rectClip, x);
  addAxes(svg, xAxis, yAxis, margin, chartWidth, chartHeight, loc, equip, index, date);
}

d3.json('/location/data?location_uuid=8639f0e6-1623-4e60-8164-a853fb807917&q_string=fromTime%3D2015-05-27 08:00:00Z%26toTime%3D2015-05-27 19:00:00Z%26interval%3Dmin%26aggregate%3dfalse%26data_format%3Drickshaw', function (error, rawData) {
  if (error) {
    console.error(error);
    return;
  }
  
  for (var i = 0; i < rawData.length; i++) {
    var data = [rawData[i]];

    var locationName = rawData.map(function(d) {
      return { loc: d.sublocation_name };
    });

    var equipmentType = rawData.map(function(d) {
      return { equip: d.equipment_type };
    });

    data.forEach(function(d){
      makeChart(d.data, locationName, equipmentType, i)
    })
  }
});
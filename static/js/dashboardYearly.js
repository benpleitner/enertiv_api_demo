d3.json("/location/data?location_uuid=8639f0e6-1623-4e60-8164-a853fb807917&q_string=fromTime%3D2015-01-01%2005:00:00Z%26toTime%3D2015-06-01%2004:00:00Z%26interval%3Dday%26aggregate%3dfalse%26data_format%3Drickshaw", function(error, rawData) {
        var monthA = new Array();
          monthA[0] = "January";
          monthA[1] = "February";
          monthA[2] = "March";
          monthA[3] = "April";
          monthA[4] = "May";
          monthA[5] = "June";
          monthA[6] = "July";
          monthA[7] = "August";
          monthA[8] = "September";
          monthA[9] = "October";
          monthA[10] = "November";
          monthA[11] = "December";

        var date = [];

        //Dates
        rawData.forEach(function(d) {
          d.data.forEach(function (d, i) {
            var time = d.x;
            var d_date = new Date(0);
            d_date.setUTCSeconds(time);
            d.x = d_date.getDate();
            d.month = monthA[d_date.getMonth()];
          });
        });

        var dateTotal = {},
            dateCost = {},
            months = [],
            max = 0,
            minDate = rawData[0].data[0].x;
            maxDate = rawData[rawData.length - 1].data[rawData[rawData.length - 1].data.length - 1].x;
            forChart = [];

        //Set the initial length
        months["lengthA"] = 0;

        rawData.forEach(function(d) {
          var name1 = d.sublocation_name

          d.data.forEach(function (d, i) {
            var name = d.x, //Change to date
                output = d.y;
                month = d.month;
                cost = d.total_cost;

            //Monthly data
            if (months[month] != null) {
              months[month].push({date: name,
                                  value: output,
                                  cost: cost});
            } else {
              months[month] = [];
              months["lengthA"]++;
              months[month].push({date: name,
                                  value: output,
                                  cost: cost});
            }

            if (name < minDate) {
              minDate = name;
            }
            if (name > maxDate) {
              maxDate = name;
            }

            if (Object.keys(dateTotal).indexOf("" + name) != -1) {
              dateTotal[name] += output;
              if (dateTotal[name] > max) {
                max = dateTotal[name];
              }
            } else {
              dateTotal[name] = output;
              if (dateTotal[name] > max) {
                max = dateTotal[name];
              }
            }
            if (Object.keys(dateCost).indexOf("" + name) != -1) {
              dateCost[name] += cost;
            } else {
              dateCost[name] = cost;
            }
          });
        });
        

        //More monthly data calculations
        var monthlyCost = [];
        var monthlyKw = []
        for (var z = 0; z < months["lengthA"]; z++) {
          var newCost = [];
          var newKw = [];
          var currentMonth = months[monthA[z]];

          for(var x = 0; x < months[monthA[z]].length; x++) {
            if (newCost[currentMonth[x].date] == null) {
              newCost[currentMonth[x].date] = currentMonth[x].cost;
              newKw[currentMonth[x].date] = currentMonth[x].value;
            } else {
              newCost[currentMonth[x].date] += currentMonth[x].cost;
              newKw[currentMonth[x].date] += currentMonth[x].value;
            }
          }
          monthlyCost.push({month: monthA[z],
                            cost: newCost});

          monthlyKw.push({month: monthA[z],
                          kw: newKw});
        }

        var arr = Object.keys(dateTotal);
        for (var i = 0; i < arr.length; i++) {
          forChart.push({ date: arr[i],
                          month: months[arr[i]],
                          value: dateTotal[arr[i]],
                          cost: dateCost[arr[i]]
                        });    
        }

        //Crossfilters
        var data = monthlyCost;
        // var data = forChart;
        // var dataJan = monthlyCost[0];
        // var dataFeb = monthlyCost[1];
        // var dataMar = monthlyCost[2];
        // var dataApr = monthlyCost[3];
        // var dataMay = monthlyCost[4];

        var ndx = crossfilter(data);

        var dates = ndx.dimension(function(d) {
          if (monthlyCost[0].month == d.month) {
            d.cost.forEach(function(d, i) {
              return i;
            })
          }
        });
        var cost = dates.group().reduceSum(function(d) {
          if (monthlyCost[0].month == d.month) {
            d.cost.forEach(function(d) {
              return d;
            })
          }
        });

        //Line Graph
        monthLineChart = dc.lineChart("#monthCost");

        monthLineChart.width(800 * 0.8)
          .height(350 * 0.8)
          .margins({top: 10, right: 50, bottom: 30, left: 40})
          .dimension(dates)
          .group(cost)
          .x(d3.scale.linear().domain([minDate, maxDate]))
          .legend(dc.legend().x(60).y(10).itemHeight(12).gap(5))
          .transitionDuration(700)
          .yAxisLabel("US Dollars")
          .xAxisLabel("Date");

        monthLineChart.xAxis().ticks(7);

        // monthLineChart.xAxis().tickFormat(function (v) {
        //   if (v == minDate - 1 || v == maxDate + 1) {
        //     return "";
        //   }
        //   return "May " + v;
        // });
});
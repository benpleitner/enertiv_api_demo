var width = 960,
    height = 500,
    radius = Math.min(width, height) / 2;

var color = d3.scale.ordinal()
    .range(["#64A8C4", "#AACEE2", "#8C65D3", "#9A93EC", "#CAB9F1", "#0052A5", "#413BF7", "#81CBF8"]);

var arc = d3.svg.arc()
    .outerRadius(radius - 20)
    .innerRadius(radius - 100);

var pie = d3.layout.pie()
    .sort(null)
    .value(function(d) { return d.value; });

var tip = d3.tip()
  .attr('class', 'd3-tip')
  .offset([0, -10])
  .html(function(d) {
    return "<strong>" + d.data.name +":</strong> <span style='color:red'>" +
      d3.format(".4f")(d.value) + " kW" + "</span>";
  });

var svg = d3.select("body").append("svg")
  .attr("width", width)
  .attr("height", height)
  .append("g")
  .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

svg.call(tip);

d3.json("/location/data?location_uuid=8639f0e6-1623-4e60-8164-a853fb807917&q_string=fromTime%3D2015-05-27 08:00:00Z%26toTime%3D2015-05-27 19:00:00Z%26interval%3Dmin%26aggregate%3dfalse%26data_format%3Drickshaw", function(error, rawData) {

    if (error) {
    console.error(error);
    return;
  }

  var averages = {},
      maxAverage = 0,
      forGraph = [];

  rawData.forEach(function(d) {
    var name = d.sublocation_name,
        data = d.data;

    var total = 0;
    var numEl = 0;
    for (var k = 0; k < data.length; k++) {
      total += data[k].y;
      numEl++;
    }

    var value = total / numEl;
    if (Object.keys(averages).indexOf(name) != -1) {
      averages[name] += value;
      if (averages[name] > maxAverage) {
        maxAverage = averages[name];
      }
    } else {
      averages[name] = value;
      if (averages[name] > maxAverage) {
        maxAverage = averages[name];
      }
    }
  });

  var arr = Object.keys(averages);
  for (var i = 0; i < arr.length; i++) {
    forGraph.push({name: arr[i],
                   value: averages[arr[i]]});    
  }

  var g = svg.selectAll(".arc")
             .data(pie(forGraph))
             .enter().append("g")
             .attr("class", "arc");

  g.append("path")
    .attr("d", arc)
    .style("fill", function(d) { return color(d.data.value); })
    .on("mouseover", tip.show)
    .on("mouseout", tip.hide)
;

  var text = g.append("text")
    .attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")"; })
    .attr("dy", ".35em")
    .style("text-anchor", "middle")
    .text(function(d) { return d.data.name; })
});